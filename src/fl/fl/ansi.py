from typing import NamedTuple

class AnsiStyle(NamedTuple):
    bld: str    # Bold
    rst: str    # Reset
    und: str    # Underline
    une: str    # Underline off


ansi_style = AnsiStyle(
    bld='\x1b[1m',     # Bold
    rst='\x1b[0m',     # Reset
    und='\x1b[4m',     # Underline
    une='\x1b[24m',    # Underline off
)


ansi_style_empty = AnsiStyle(
    bld='',     # Bold
    rst='',     # Reset
    und='',     # Underline
    une='',     # Underline off
)


class AnsiForeground(NamedTuple):
    blk: str    # Foreground Black
    blu: str    # Foreground Blue
    cyn: str    # Foreground Cyan
    grn: str    # Foreground Green
    mgt: str    # Foreground Magenta
    org: str    # Foreground Orange
    prp: str    # Foreground Purple
    red: str    # Foreground Red
    wht: str    # Foreground White
    ylw: str    # Foreground Yellow
    dft: str    # Foreground Default Color


ansi_foreground = AnsiForeground(
    blk='\x1b[30m',          # Foreground Black
    blu='\x1b[34m',          # Foreground Blue
    cyn='\x1b[36m',          # Foreground Cyan
    grn='\x1b[32m',          # Foreground Green
    mgt='\x1b[35m',          # Foreground Magenta
    org='\x1b[38;5;209m',    # Foreground Orange
    prp='\x1b[38;5;93m',     # Foreground Purple
    red='\x1b[31m',          # Foreground Red
    wht='\x1b[37m',          # Foreground White
    ylw='\x1b[33m',          # Foreground Yellow
    dft='\x1b[39m',          # Foreground Default Color
)


ansi_foreground_empty = AnsiForeground(
    blk='',     # Foreground Black
    blu='',     # Foreground Blue
    cyn='',     # Foreground Cyan
    grn='',     # Foreground Green
    mgt='',     # Foreground Magenta
    org='',     # Foreground Orange
    prp='',     # Foreground Purple
    red='',     # Foreground Red
    wht='',     # Foreground White
    ylw='',     # Foreground Yellow
    dft='',     # Foreground Default Color
)


# Background
class AnsiBackground(NamedTuple):
    blk: str    # Background Black
    blu: str    # Background Blue
    cyn: str    # Background Cyan
    grn: str    # Background Green
    mgt: str    # Background Magenta
    org: str    # Background Orange
    prp: str    # Background Purple
    red: str    # Background Red
    wht: str    # Background White
    ylw: str    # Background Yellow
    dft: str    # Background Default Color


ansi_background = AnsiBackground(
    blk='\x1b[40m',          # Background Black
    blu='\x1b[44m',          # Background Blue
    cyn='\x1b[46m',          # Background Cyan
    grn='\x1b[42m',          # Background Green
    mgt='\x1b[45m',          # Background Magenta
    org='\x1b[48;5;209m',    # Background Orange
    prp='\x1b[48;5;93m',     # Background Purple
    red='\x1b[41m',          # Background Red
    wht='\x1b[47m',          # Background White
    ylw='\x1b[43m',          # Background Yellow
    dft='\x1b[49m',          # Background Default Color
)


ansi_background_empty = AnsiBackground(
    blk='',     # Background Black
    blu='',     # Background Blue
    cyn='',     # Background Cyan
    grn='',     # Background Green
    mgt='',     # Background Magenta
    org='',     # Background Orange
    prp='',     # Background Purple
    red='',     # Background Red
    wht='',     # Background White
    ylw='',     # Background Yellow
    dft='',     # Background Default Color
)


class Ansi(NamedTuple):
    st: AnsiStyle
    fg: AnsiForeground
    bg: AnsiBackground


def get_ansi_style(no_color: bool = False) -> AnsiStyle:
    # Returns the ansi style codes as a namedtuple
    if no_color is True:
        return ansi_style_empty
    return ansi_style


def get_ansi_foreground(no_color: bool = False) -> AnsiForeground:
    # Returns the ansi foreground color codes as a namedtuple
    if no_color is True:
        return ansi_foreground_empty
    return ansi_foreground


def get_ansi_background(no_color: bool = False) -> AnsiBackground:
    # Returns the ansi background color codes as a namedtuple
    if no_color is True:
        return ansi_background_empty
    return ansi_background


def get_ansi(no_color: bool = False) -> Ansi:
    return Ansi(
        get_ansi_style(no_color=no_color),
        get_ansi_foreground(no_color=no_color),
        get_ansi_background(no_color=no_color)
    )
