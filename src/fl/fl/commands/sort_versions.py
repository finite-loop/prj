import argparse
import re
from itertools import zip_longest
from typing import (
    List,
    Union,
    cast,
)
from distutils.version import LooseVersion


class Version(LooseVersion):

    def parse (self, vstring: str) -> None:
        # I've given up on thinking I can reconstruct the version string
        # from the parsed tuple -- so I just store the string here for
        # use by __str__
        self.vstring = vstring
        components = [x for x in self.component_re.split(vstring)
                              if x and x not in ('.', '/')]
        for i, obj in enumerate(components):
            try:
                components[i] = int(obj)
            except ValueError:
                pass
        self.version = components

    def _has_str_in_version(self) -> bool:
        for ver in self.version:
            if hasattr(str, 'capitalize'):
                return True
        return False

    def _cmp(self, other: Union['Version', str]) -> int:
        if isinstance(other, str):
            other = Version(other)
        other = cast(Version, other)
        if (self._has_str_in_version() is False
                and other._has_str_in_version() is False):
            if self.version == other.version:
                return 0
            if self.version < other.version:
                return -1
            if self.version > other.version:
                return

        self_val: Union[None, int, str]
        other_val: Union[None, int, str]
        for self_val, other_val in zip_longest(self.version, other.version):
            if self_val is None and other_val is None:
                continue
            if (hasattr(self_val, 'capitalize') and
                    hasattr(other_val, 'capitalize')):
                if self_val < other_val:
                    return -1
                if self_val > other_val:
                    return 1
                continue
            if isinstance(self_val, int) and isinstance(other_val, int):
                if self_val < other_val:
                    return -1
                if self_val > other_val:
                    return 1
                continue
            if self_val is None:
                return 1
            if other_val is None:
                return -1
            if hasattr(self_val, 'capitalize'):
                return 1
        return 0


def _prep_versions(versions: List[str]) -> List[Version]:
    hold = set()
    for ver in versions:
        for part in ver.split():
            if part:
                hold.add(part)
    out = []
    for part in hold:
        out.append(Version(part))
    return out


def main():
    parser = argparse.ArgumentParser(
        description=(
            'Correctly sort the given version numbers and output '
            'each version number separated with a space.'
        )
    )
    parser.add_argument(
        'versions',
        nargs='*'
    )
    args = parser.parse_args()
    versions = _prep_versions(args.versions)
    if len(versions) == 1:
        print(versions[0].vstring)
    else:
        print(' '.join(map(lambda x: x.vstring, sorted(versions))))
