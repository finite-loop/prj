import argparse
import os
from typing import (
    Dict,
    List,
    Optional,
    Tuple,
    Union,
)

import gitlab
from flutils.decorators import cached_property
from gitlab.v4.objects import (
    Project,
    ProjectRegistryRepository,
)

_GITLAB_PROJECT = 'finite-loop/docker-pyenv-nvm'
_CONFIG_FILE = os.path.realpath(os.path.dirname(__file__))
_CONFIG_FILE = os.path.dirname(_CONFIG_FILE)
_CONFIG_FILE = os.path.dirname(_CONFIG_FILE)
_CONFIG_FILE = os.path.dirname(_CONFIG_FILE)
_CONFIG_FILE = os.path.dirname(_CONFIG_FILE)
_CONFIG_FILE = os.path.join(_CONFIG_FILE, 'config-gitlab')
_CONFIG_SECTION = 'gitlab'

def _build_key(text: str) -> Tuple[Union[str, int], ...]:
    parts = text.split('-')
    out = []
    for part in parts:
        for sub in part.split('.'):
            sub: str
            if sub.isdigit():
                out.append(int(sub))
            else:
                out.append(sub)
    return tuple(out)


class Process:

    def __init__(
        self,
        config_file: str,
        config_section: str,
        gitlab_project: str,
        registry_name: str,
    ) -> None:
        self.config_file: str = config_file
        self.config_section: str = config_section
        self.gitlab_project: str = gitlab_project
        self.registry_name: str = registry_name

    @cached_property
    def _api(self) -> gitlab.Gitlab:
        return gitlab.Gitlab.from_config(
            self.config_section,
            [self.config_file]
        )

    @cached_property
    def project(self) -> Project:
        return self._api.projects.get(self.gitlab_project)

    @cached_property
    def registry(self) -> Optional[ProjectRegistryRepository]:
        for repository in self.project.repositories.list():
            if repository.name == self.registry_name:
                return repository

    @cached_property
    def location(self) -> str:
        if self.registry is None:
            return ''
        return self.registry.location

    @cached_property
    def tags(self) -> List[str]:
        if self.registry is None:
            return []
        hold: Dict[Tuple[Union[str, int], ...], str] = {}
        for tag in self.registry.tags.list():
            name = tag.name
            key = _build_key(name)
            val = '%s:%s' % (self.location, name)
            hold[key] = val
        out = []
        for key in sorted(hold.keys()):
            out.append(hold[key])
        return out

def main():
    config_file = os.getenv('CONFIG_FILE', _CONFIG_FILE)
    config_section = os.getenv('CONFIG_SECTION', _CONFIG_SECTION)
    gitlab_project = os.getenv('GITLAB_PROJECT', _GITLAB_PROJECT)

    parser = argparse.ArgumentParser(
        description="Show the GitLab registry tags for the given name."
    )
    parser.add_argument(
        '--config-file',
        default=config_file,
        help=(
            'The path to the config file.  Defaults to: %r'
            % config_file
        )
    )
    parser.add_argument(
        '--config-section',
        default=config_section,
        help=(
            'The section of the config file to use. Defaults to: %r'
            % config_section
        )
    )
    parser.add_argument(
        '--gitlab-project',
        default=gitlab_project,
        help=(
            'The <user>/<project> project name. Defaults to: %r'
            % gitlab_project
        )
    )
    parser.add_argument(
        'registry_name',
        help=(
            'The name of the registry. (does not contain any slashes) '
            'Look at https://gitlab.com/%s/container_registry the '
            'registry name is the word following the last slash. (e.g. '
            '%s/<registry_name>)' % (
                gitlab_project,
                gitlab_project
            )
        )
    )
    args = parser.parse_args()
    process = Process(
        args.config_file,
        args.config_section,
        args.gitlab_project,
        args.registry_name
    )
    for name in process.tags:
        print(name)

if __name__ == "__main__":
    main()
