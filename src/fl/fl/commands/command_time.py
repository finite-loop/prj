import argparse
from typing import (
    List,
    NamedTuple,
)



class ResultTime(NamedTuple):
    days: int
    hours: int
    minutes: int
    seconds: int

_day_seconds = 86400
_hour_seconds = 3600
_minute_seconds = 60

def calculate(start_time: int, end_time: int) -> ResultTime:
    args = []
    seconds: int = end_time - start_time
    if seconds >= _day_seconds:
        args.append(int(seconds / _day_seconds))
        seconds = seconds % _day_seconds
    else:
        args.append(0)

    if seconds >= _hour_seconds:
        args.append(int(seconds / _hour_seconds))
        seconds = seconds % _hour_seconds
    else:
        args.append(0)

    if seconds >= _minute_seconds:
        args.append(int(seconds / _minute_seconds))
        seconds = seconds % _minute_seconds
    else:
        args.append(0)
    args.append(seconds)
    return ResultTime(*args)

def build_output(obj: ResultTime) -> str:
    out: List[str] = []
    days_added = False
    if obj.days > 0:
        if obj.days == 1:
            out.append('1 day')
        else:
            out.append('%s days' % obj.days)
        days_added = True

    hours_added = False
    if obj.hours > 0:
        if obj.hours == 1:
            out.append('1 hour')
        else:
            out.append('%s hours' % obj.hours)
        hours_added = True
    else:
        if days_added is True:
            out.append('0 hours')
            hours_added = True

    minutes_added = False
    if obj.minutes > 0:
        if obj.minutes == 1:
            out.append('1 minute')
        else:
            out.append('%s minutes' % obj.minutes)
        minutes_added = True
    else:
        if hours_added is True:
            out.append('0 minutes')
            minutes_added = True

    if obj.seconds > 0:
        if obj.seconds == 1:
            out.append('1 second')
        else:
            out.append('%s seconds' % obj.seconds)
    else:
        if minutes_added is True:
            out.append('0 seconds')
        else:
            out.append('under 1 second')
    if len(out) == 1:
        return out[0]
    last = out.pop(-1)
    return '%s and %s' % (', '.join(out), last)

def main():
    parser = argparse.ArgumentParser(
        description=(
            "Display a readable execution time from the given start-time "
            "(in seconds) and end-time (in seconds)"
        )
    )
    parser.add_argument(
        'start_time',
        type=int,
        help='The start-time in seconds'
    )
    parser.add_argument(
        'end_time',
        type=int,
        help='The end-time in seconds'
    )
    args = parser.parse_args()
    if args.start_time > args.end_time:
        raise SystemExit(
            'The given start_time: %r is greater than the given '
            'end_time: %r' % (args.start_time, args.end_time)
        )
    obj = calculate(args.start_time, args.end_time)
    output = build_output(obj)
    print(output)



