from pprint import pprint
import re
import functools
from typing import (
    Dict,
    Generator,
    Optional,
    Tuple,
)
MAIN_SECTION: str = 'general'
SENTINEL_DEFAULT: str = 'HELP'
SENTINEL_KEYS: Tuple[str, ...] = (
    'USAGE:',
    'SUMMARY:',
    'DESCRIPTION:',
    'EPILOG:',
)
SENTINEL_SECTIONS: Tuple[str, ...] = (
    '[[OPTIONS]]',
    '[[ARGUMENTS]]',
)


class ExtractHelp:

    def __init__(
            self,
            main_section: Optional[str] = None,
            sentinel: Optional[str] = None
    ) -> None:
        self.main_section: str = main_section or MAIN_SECTION
        self._sentinel: str = sentinel or SENTINEL_DEFAULT

    @staticmethod
    def _each_line(
            sentinel: str,
            file_path: str,
    ) -> Generator[str, None, None]:
        sentinel_start = ": << '%s'" % sentinel
        sentinel_end = sentinel

        started = False
        with open(file_path, 'r') as fp:
            for line in fp:
                line = line.rstrip()
                if started is False:
                    if line == sentinel_start:
                        started = True
                        continue
                else:
                    if line == sentinel_end:
                        break
                    yield line

    def __call__(self, file_path: str) -> Dict[str, Dict[str, str]]:
        main_section = self.main_section
        out: Dict[str, Dict[str, str]] = {
            main_section: {
                'usage': '',
                'summary': '',
                'description': '',
                'epilog': '',
            },
            'options': {},
            'arguments': {}

        }
        current_section: str = ''
        current_key: str = ''
        sentinel_keys = SENTINEL_KEYS
        sentinel_sections = SENTINEL_SECTIONS
        for line in self._each_line(self._sentinel, file_path):
            if line:
                if line.startswith(' ') is False:
                    if line in sentinel_keys:
                        current_section = main_section
                        current_key = line[:-1].lower()
                        continue
                    if line in sentinel_sections:
                        current_section = line[2:-2].lower()
                        current_key = ''
                        continue
                    if current_section == 'options':
                        if line.startswith('-') and line.endswith(':'):
                            current_key = line[:-1]
                        else:
                            current_key = ''
                        continue
                    if current_section == 'arguments':
                        if line.endswith(':'):
                            current_key = line[:-1]
                        else:
                            current_key = ''
                    continue
            if current_section and current_key:
                if current_section not in out:
                    out[current_section] = {}
                if current_key not in out[current_section]:
                    out[current_section][current_key] = ''

                val = out[current_section][current_key]
                if line:
                    if line.startswith('  '):
                        line = line[2:].rstrip()
                    else:
                        continue
                if val:
                    out[current_section][current_key] = '%s\n%s' % (val, line)
                else:
                    if line:
                        out[current_section][current_key] = line
        return out


