import re
import os
from enum import Enum
from textwrap import dedent
from typing import (
    Any,
    Dict,
    Generator,
    List,
    NamedTuple,
    Optional,
    Set,
    Tuple,
    Type,
    Union,
    cast,
)
from fl.ansi import (
    Ansi,
    AnsiBackground,
    AnsiForeground,
    AnsiStyle,
)
from ..txtutils import text_without_ansi


class ParagraphType(Enum):
    EMPTY = 0
    NORMAL = 1
    LIST_ORDERED = 2
    LIST_UNORDERED = 3


class InterpolationValues(NamedTuple):
    cmd: str
    sub_cmd: str
    full_cmd: str
    st: AnsiStyle
    fg: AnsiForeground
    bg: AnsiBackground
    base_docker_repo: str
    cmd_invoke: str
    command: str
    command_config_file: str
    command_home: str
    command_bin_home_dollar: str
    command_lib_home: str
    command_libexec_home: str
    command_src_home: str
    command_template_home: str
    command_update_use: str
    command_venv_home: str
    github_config_file: str
    gitlab_config_file: str
    project_name: str
    project_home: str
    project_command_home: str
    project_command_libexec_home: str


def _each_field(cls: Type[NamedTuple]) -> Generator[str, None, None]:
    for field in sorted(cls._fields):
        yield field


_field_classes = {
    'st': AnsiStyle,
    'fg': AnsiForeground,
    'bg': AnsiBackground,
}


class InterpolationError(SystemExit):

    def __init__(self, msg: str):
        msg = (
            'There is an error when trying to parse the help section.\n\n'
            '%s\n\nThe following interpolation keys and attributes are '
            'available:' % msg
        )
        out = []
        for field in _each_field(InterpolationValues):
            if field in ('st', 'fg', 'bg'):
                for f in _each_field(_field_classes[field]):
                    out.append('  %s.%s' % (field, f))
            else:
                out.append('  %s' % field)
        msg = '%s\n%s\n\n' % (msg, '\n'.join(out))
        super().__init__(msg)


_home = os.path.expanduser('~')
_home_len = len(_home)


def _replace_home(text: str, use_dollar_home=False) -> str:
    if text.startswith(_home):
        if use_dollar_home is True:
            return '${{HOME}}{}'.format(
                text[_home_len:]
            )
        else:
            return '~%s' % text[_home_len:]
    return text


env_kwargs = {
    'cmd': os.getenv(
        'CMD',
        '[[CMD NOT SET]]'
    ),
    'base_docker_repo': os.getenv(
        'BASE_DOCKER_REPOSITORY',
        '[[BASE_DOCKER_REPOSITORY NOT SET]]'
    ),
    'cmd_invoke': _replace_home(os.getenv(
        'CMD_INVOKE',
        '[[CMD_INVOKE NOT SET]]'
    )),
    'command': os.getenv(
        'COMMAND',
        '[[COMMAND NOT SET]]'
    ),
    'command_config_file': _replace_home(os.getenv(
        'COMMAND_CONFIG_FILE',
        '[[COMMAND_CONFIG_FILE NOT SET]]'
    )),
    'command_home': _replace_home(os.getenv(
        'COMMAND_HOME',
        '[[COMMAND_HOME NOT SET]]'
    )),
    'command_bin_home_dollar': _replace_home(
        os.getenv(
            'COMMAND_BIN_HOME',
            '[[COMMAND_BIN_HOME NOT SET]]'
        ),
        use_dollar_home=True
    ),

    'command_lib_home': _replace_home(os.getenv(
        'COMMAND_LIB_HOME',
        '[[COMMAND_LIB_HOME NOT SET]]'
    )),
    'command_libexec_home': _replace_home(os.getenv(
        'COMMAND_LIBEXEC_HOME',
        '[[COMMAND_LIBEXEC_HOME NOT SET]]'
    )),
    'command_src_home': _replace_home(os.getenv(
        'COMMAND_SRC_HOME',
        '[[COMMAND_SRC_HOME NOT SET]]'
    )),
    'command_template_home': _replace_home(os.getenv(
        'COMMAND_TEMPLATE_HOME',
        '[[COMMAND_TEMPLATE_HOME NOT SET]]'
    )),
    'command_update_use': _replace_home(os.getenv(
        'COMMAND_UPDATE_USE',
        'latest'
    )),
    'command_venv_home': _replace_home(os.getenv(
        'COMMAND_VENV_HOME',
        '[[COMMAND_VENV_HOME NOT SET]]'
    )),
    'github_config_file': _replace_home(os.getenv(
        'GITHUB_CONFIG_FILE',
        '[[GITHUB_CONFIG_FILE NOT SET]]'
    )),
    'gitlab_config_file': _replace_home(os.getenv(
        'GITLAB_CONFIG_FILE',
        '[[GITLAB_CONFIG_FILE NOT SET]]'
    )),
    'project_name': _replace_home(os.getenv(
        'PROJECT_NAME',
        '[[PROJECT_NAME NOT SET]]'
    )),
    'project_home': _replace_home(os.getenv(
        'PROJECT_HOME',
        '[[PROJECT_HOME NOT SET]]'
    )),
    'project_command_home': _replace_home(os.getenv(
        'PROJECT_COMMAND_HOME',
        '[[PROJECT_COMMAND_HOME NOT SET]]'
    )),
    'project_command_libexec_home': _replace_home(os.getenv(
        'PROJECT_COMMAND_LIBEXEC_HOME',
        '[[PROJECT_COMMAND_LIBEXEC_HOME NOT SET]]'
    )),

}

def build_interpolation_values(
        cmd: str,
        sub_cmd: str,
        full_cmd: str,
        ansi: Ansi,
        **kwargs: Any
) -> InterpolationValues:
    _in_kwargs = dict(kwargs)
    for key in _in_kwargs:
        _in_kwargs[key] = _replace_home(kwargs[key])
    _kwargs = dict(env_kwargs)
    _kwargs.update(_in_kwargs)
    _kwargs['cmd'] = cmd
    _kwargs['sub_cmd'] = sub_cmd
    _kwargs['full_cmd'] = full_cmd
    _kwargs['st'] = ansi.st
    _kwargs['fg'] = ansi.fg
    _kwargs['bg'] = ansi.bg
    # Remove any invalid keys
    for key in _kwargs.keys():
        if key not in InterpolationValues._fields:
            del _kwargs[key]
    return InterpolationValues(**_kwargs)

class TextObj(NamedTuple):
    text: str
    indent_len: int
    initial_indent: str
    initial_len: int
    subsequent_indent: str
    subsequent_len: int
    ptype: ParagraphType


_re_line_normal = re.compile(r'^(\s*)(.*)$')
_re_line_list_ordered = re.compile(r'^(\s*)([0-9]{1,3}\.\s*)(\S.*)$')
_re_line_list_unordered = re.compile(r'^(\s*)(\*\s*)(\S.*)$')

_ANSI_RE = re.compile('(\x1b\\[[0-9;:]+[ABCDEFGHJKSTfhilmns])')


def _indent_count(clean_part: str) -> int:
    out = 0
    if clean_part:
        for c in clean_part:
            if c == ' ':
                out += 1
            else:
                break
    return out


def _each_char(line: str) -> Generator[str, None, None]:
    for part in _ANSI_RE.split(line):
        if part:
            if _ANSI_RE.findall(part):
                yield part
            else:
                for c in part:
                    yield c


def _build_ordered(line: str) -> Tuple[str, str]:
    indent = ''
    indent_finished = False
    period_finished = False
    text = ''
    for c in _each_char(line):
        if indent_finished is False:
            if c == '.':
                period_finished = True
            else:
                if period_finished is True and c == ' ':
                    indent_finished = True
            indent += c
        else:
            text += c
    return indent, text


def _build_unordered(line: str) -> Tuple[str, str]:
    indent = ''
    indent_finished = False
    star_finished = False
    text = ''
    for c in _each_char(line):
        if indent_finished is False:
            if c == '*':
                star_finished = True
            else:
                if star_finished is True and c == ' ':
                    indent_finished = True
            indent += c
        else:
            text += c
    return indent, text


def _build_normal(line: str) -> Tuple[str, str]:
    indent = ''
    indent_finished = False
    text = ''
    for c in _each_char(line):
        if indent_finished is False:
            if c == ' ':
                indent += c
            else:
                indent_finished = True
                text += c
        else:
            text += c
    return indent, text


def _match_line(line: str) -> Tuple[str, str]:
    clean_line = text_without_ansi(line)
    skip = False
    if clean_line.lstrip().startswith('\\'):
        line = line.replace('\\', '', 1)
        skip = True

    if skip is False:
        # Match ordered list.
        _match = _re_line_list_ordered.match(clean_line)
        if _match is not None:
            if clean_line == line:
                indent, mid, text = _match.groups()
                indent = '%s%s' % (indent, mid)
            else:
                indent, text = _build_ordered(line)
            return indent, text, ParagraphType.LIST_ORDERED

        # Match unordered list.
        _match = _re_line_list_unordered.match(clean_line)
        if _match is not None:
            if clean_line == line:
                indent, mid, text = _match.groups()
                indent = '%s%s' % (indent, mid)
            else:
                indent, text = _build_unordered(line)
            return indent, text, ParagraphType.LIST_UNORDERED
    # Everything else
    indent, text = _build_normal(line)
    return indent, text, ParagraphType.NORMAL


def _build_text_obj_kwargs(line: str) -> Optional[Dict[str, Any]]:
    kwargs = {
        'text': '',
        'indent_len': 0,
        'initial_indent': '',
        'initial_len': 0,
        'subsequent_indent': '',
        'subsequent_len': 0,
        'ptype': ParagraphType.EMPTY
    }
    if not line:
        return kwargs

    initial_indent, text, ptype = _match_line(line)
    if not initial_indent and not text:
        return None

    clean_initial_indent = text_without_ansi(initial_indent)
    kwargs['text'] = text
    kwargs['indent_len'] = _indent_count(clean_initial_indent)
    kwargs['initial_indent'] = initial_indent
    kwargs['initial_len'] = len(clean_initial_indent)
    kwargs['subsequent_indent'] = ' ' * kwargs['initial_len']
    kwargs['subsequent_len'] = kwargs['initial_len']
    kwargs['ptype'] = ptype
    return kwargs

def _merge_into_previous(
        previous: Dict[str, str],
        current: Dict[str, str]
) -> None:
    previous['text'] = previous['text'].strip()
    current_initial_indent = current['initial_indent'].strip()
    if current_initial_indent:
        previous['text'] += ' '
        previous['text'] += current_initial_indent
    current_text = current['text'].strip()
    if current_text:
        previous['text'] += ' '
        previous['text'] += current_text


def _each_line(text: str) -> Generator[TextObj, None, None]:
    """Yield a text object for each line of the given ``text``.
    """
    previous = None
    for line in text.splitlines():
        current = _build_text_obj_kwargs(line)
        if current is None:
            continue
        if previous is None:
            previous = current
            continue
        pre_sub_len = previous['subsequent_len']
        cur_ini_len = current['initial_len']
        if previous['ptype'] == current['ptype']:
            if previous['ptype'] == ParagraphType.EMPTY:
                continue
            if previous['ptype'] == ParagraphType.NORMAL:
                if pre_sub_len == cur_ini_len:
                    _merge_into_previous(previous, current)
                    continue
                else:
                    yield TextObj(**previous)
                    previous = current
                    continue
            if previous['ptype'] == ParagraphType.LIST_ORDERED:
                yield TextObj(**previous)
                previous = current
                continue
            if previous['ptype'] == ParagraphType.LIST_UNORDERED:
                yield TextObj(**previous)
                previous = current
                continue
        else:
            if (previous['ptype'] == ParagraphType.EMPTY or
                    current['ptype'] == ParagraphType.EMPTY):
                yield TextObj(**previous)
                previous = current
                continue
            if current['ptype'] == ParagraphType.NORMAL:
                if pre_sub_len == cur_ini_len:
                    _merge_into_previous(previous, current)
                    continue
            yield TextObj(**previous)
            previous = current
            continue
    if previous:
        if previous['ptype'] != ParagraphType.EMPTY:
            yield TextObj(**previous)


def interpolate(
    _text_: str,
    interpolation_values: Optional[InterpolationValues] = None,
    **kwargs: Any
) -> str:
    if interpolation_values is None:
        _kwargs = dict(kwargs)
    else:
        _kwargs = interpolation_values._asdict()
        _kwargs.update(kwargs)
    try:
        return _text_.format(**_kwargs)
    except AttributeError as e:
        raise InterpolationError(str(e))
    except KeyError as e:
        raise InterpolationError('Unknown key: %s' % str(e))


def each_paragraph(
            _text_: str,
            interpolation_values: InterpolationValues
    ) -> Generator[TextObj, None, None]:
        _text_ = dedent(_text_)
        _text_ = _text_.strip()
        _text_ = interpolate(_text_, interpolation_values)
        for line in _each_line(_text_):
            yield line
