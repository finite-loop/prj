import re
from typing import (
    Dict,
    List,
    Optional,
    Tuple,
    Union,
    cast
)
from .utils import WIDTH
from ..txtutils import AnsiTextWrapper


WRAPPERS: Dict[Tuple[str, str], AnsiTextWrapper] = {
    ('', ''): AnsiTextWrapper(
        initial_indent='',
        subsequent_indent='',
        width=WIDTH
    )
}


def get_wrapper(
        initial: Union[int, str] = 0,
        subsequent: Optional[Union[int, str]] = None,
) -> AnsiTextWrapper:
    """Return an AnsiTextWrapper object for wrapping text."""
    if hasattr(initial, 'capitalize'):
        initial_indent = initial
    else:
        initial_indent = ' ' * initial
    initial_indent = cast(str, initial_indent)

    if subsequent is None:
        subsequent = len(initial_indent)

    if hasattr(subsequent, 'capitalize'):
        subsequent_indent = subsequent
    else:
        subsequent_indent = ' ' * subsequent

    subsequent_indent = cast(str, subsequent_indent)

    key = (initial_indent, subsequent_indent)

    if key not in WRAPPERS:
        WRAPPERS[key] = AnsiTextWrapper(
            initial_indent=initial_indent,
            subsequent_indent=subsequent_indent,
            width=WIDTH
        )
    return WRAPPERS[key]
