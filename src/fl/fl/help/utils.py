import os
from typing import (
    Dict,
    Optional,
    Tuple,
)
from textwrap import TextWrapper


def fetch_terminal_columns() -> int:
    """Return the number of terminal columns."""
    _, _, size = os.popen('stty size', 'r').read().strip().partition(' ')
    try:
        out = int(size) - 2
    except (TypeError, ValueError):
        out = 68
    if out < 48:
        out = 48
    return out


WIDTH = fetch_terminal_columns()


class GetWrapper:

    def __init__(self) -> None:
        self.width: int = fetch_terminal_columns()
        self._wrappers: Dict[Tuple[int, int], TextWrapper] = {
            (0, 0): TextWrapper(
                initial_indent='',
                subsequent_indent='',
                width=self.width
            )
        }

    def __call__(
            self,
            indent_initial: int = 0,
            indent_subsequent: Optional[int] = None,
    ) -> TextWrapper:
        """Return a TextWrapper object for wrapping text."""

        if indent_subsequent is None:
            indent_subsequent = indent_initial

        key = (indent_initial, indent_subsequent)
        if key not in self._wrappers:
            indent_initial_text = ' ' * indent_initial
            indent_subsequent_text = ' ' * indent_subsequent
            width = self.width
            wrapper = TextWrapper(
                initial_indent=indent_initial_text,
                subsequent_indent=indent_subsequent_text,
                width=width
            )
            self._wrappers[key] = wrapper
        return self._wrappers[key]


get_wrapper = GetWrapper()
