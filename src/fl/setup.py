from setuptools import setup

kwargs = {}
kwargs['entry_points'] = {}
kwargs['entry_points']['console_scripts'] = [
    'command_time = fl.commands.command_time:main',
    'gitlab_registry_tags = fl.commands.gitlab_registry_tags:main',
    'sort_versions = fl.commands.sort_versions:main',
]
setup(**kwargs)
