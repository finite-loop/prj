#! /usr/bin/env sh
[[ ! -x "/opt/poetry/bin/poetry" ]] && return

export PATH="/opt/poetry/bin:${PATH}"

[[ -z "${PS1}" ]] && return

[[ -z "${SHELL}" ]] && return

if [[ "$(basename "${SHELL}")" == *sh ]]; then
  eval "$(/opt/poetry/bin/poetry completions bash 2> /dev/null)"
fi
