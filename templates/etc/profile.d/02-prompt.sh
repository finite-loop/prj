#! /usr/bin/env sh

# If not running interactively, don't do any of the following
[[ -z "$PS1" ]] && return

if [[ "${USER}" == 'root' ]]; then
  export PS1="\[\e[1;31m\]\u\[\e[m\]\[\e[1;34m\]: \w\[\e[m\]\[\e[0;37m\]\$\[\e[m\] "
else
  export PS1="\[\e[1;32m\]\u\[\e[m\]\[\e[1;34m\]: \w\[\e[m\]\[\e[0;37m\]\$\[\e[m\] "
fi


if [[ -x /usr/bin/dircolors ]]; then
  if [[ -r "${HOME}/.dircolors" ]]; then
    eval "$(/usr/bin/dircolors -b "${HOME}/.dircolors")"
  else
    eval "$(/usr/bin/dircolors -b)"
  fi
  alias ls='ls --color=auto'
  alias ll='ls -Flap --color=auto'
  alias la='ls -A --colo=auto'
  alias l='ls -CF --color=auto'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi
