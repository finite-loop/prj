#! /usr/bin/env sh
[[ ! -x "/opt/pyenv/bin/pyenv" ]] && return
export PATH="/opt/pyenv/bin:${PATH}"

if [[ -d "/opt/pyenv/shims" ]]; then
  export PATH="/opt/pyenv/shims:${PATH}"
fi

[[ -z "${PS1}" ]] && return

[[ -z "${SHELL}" ]] && return

[[ ! -r /opt/pyenv/completions/pyenv.bash ]] && return

if [[ "$(basename "${SHELL}")" == *sh ]]; then
  \. /opt/pyenv/completions/pyenv.bash
fi
