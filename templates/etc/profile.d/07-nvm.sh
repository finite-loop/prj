#! /usr/bin/env sh

[[ ! -r "/opt/nvm/nvm.sh" ]] && return

export NVM_NODEJS_ORG_MIRROR='https://unofficial-builds.nodejs.org/download/release'
export NVM_DIR='/opt/nvm'
if [[ -s "${NVM_DIR}/nvm.sh" ]]; then
  \. "${NVM_DIR}/nvm.sh" 2>&1 /dev/null || true
fi

nvm_get_arch() { nvm_echo "x64-musl"; }
