#! /usr/bin/env sh
export MUSL_LOCPATH=/usr/share/i18n/locales/musl
export CHARSET=UTF-8
# LANG=C.UTF-8
export LANG=en_US.UTF-8
export LC_COLLATE=C
