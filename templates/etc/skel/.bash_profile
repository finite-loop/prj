# Load /etc/profile
\. /etc/profile

# Load ~/.profile if it exists
if [[ -r "${HOME}/.profile" ]]; then
  \. "${HOME}/.profile"
fi

export FROM_BASH_PROFILE=true

# Load ~/.bashrc
if [[ -r "${HOME}/.bashrc" ]]; then
  \. "${HOME}/.bashrc"
fi
