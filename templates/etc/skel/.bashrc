if [[ -z "${FROM_BASH_PROFILE}" ]]; then
  # Load /etc/profile
  \. /etc/profile

  # Load ~/.profile if it exists
  if [[ -r "${HOME}/.profile" ]]; then
    \. "${HOME}/.profile"
  fi
fi

# If not running interactively, don't do anything
[[ -z "$PS1" ]] && return

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Alias definitions can be in separate files within ~/.bash_aliases/
if [[ -d "${HOME}/.bash_aliases" ]]; then
  for f in $(find ~/.bash_aliases/ -mindepth 1 -type f -name "*.sh"); do
    \. $f
  done
fi

# Completions can be in separate files within ~/.bash_completions/
if [[ -d "${HOME}/.bash_completions" ]]; then
  for f in $(find ~/.bash_completions/ -mindepth 1 -type f -name "*.sh"); do
    . $f
  done
fi
